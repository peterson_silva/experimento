class Credito00Cest
{

    public function creditoDadosOpcional(AcceptanceTester $I)
    {
        $I->wantTo('Verificar se o cadastro de informações opcionais de credito do cliente está correto');
        $I->amOnPage('/pessoa-fisica/update?id=1&active=1');
        $I->click('Crédito');
        $I->waitForText('Limite de crédito');

        $I->fillField('#pessoafisica-limite_de_credito-disp', '5000,00');
        $I->fillField('#pessoafisica-n_max_parcelas', '10');

        $I->click('//*[@id="w27"]/div[2]/button');

        $I->waitForText("Cliente (pessoa fisíca) atualizada com sucesso!");

        $I->click('Crédito');
        $I->waitForText('Limite de crédito');

        $I->seeInField('#pessoafisica-limite_de_credito-disp', "R$ 5.000");
        $I->seeInField('#pessoafisica-n_max_parcelas', '10');
    }
}
